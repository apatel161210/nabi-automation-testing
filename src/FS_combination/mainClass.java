package FS_combination;

public class mainClass {

    public static  void runJenkinstest() throws  InterruptedException{
        FS_jenkinsTest fsJenkinsTest = new FS_jenkinsTest();
        fsJenkinsTest.checkEmailSubscription();
        fsJenkinsTest.checkapplicationListPage();
        fsJenkinsTest.checkConnectFormPage();
        fsJenkinsTest.checkRequestCatalogPageUsa();
        fsJenkinsTest.checkRequestCatalogPageCanada();
        fsJenkinsTest.checkRequestToolkitPage();
        fsJenkinsTest.checkEdConsultantPage();
        fsJenkinsTest.checkSnapchatPage();
    }
    public static void main( String [] args) throws  InterruptedException{
        runJenkinstest();
    }
}
