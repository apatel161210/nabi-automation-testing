package FS_requestToolkit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;

public class FS_requestToolkitCanada {

    WebDriver driver;

    //@Test(priority = 0)
    public void launchBrowser(WebDriver driver) {

        Reporter.log("This test will verify the firefox browser launch");

        System.out.println("launching firefox browser");
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.home") + "/bin/geckodriver");
        driver = new FirefoxDriver();
    }

    //Method is to open the "Readyformore.com website's contact us form" in the web browser
    //@Test(priority = 1)
    public void openApplication(WebDriver driver) throws InterruptedException {

        Reporter.log("This test will open the fusionSpan website on firefox");
        driver.get("https://readyformore.com/ed-consultants");



    }
    //Method is for entering data into "Email Address" field
    //@Test(priority = 2)
    public void enteremail(WebDriver driver, String date) {
        Reporter.log("Entering Email address");
        WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                email);
        email.click();
        email.clear();
        String emailID = "spudari+" + date + "reqtoolkit@fusionspan.com";
        email.sendKeys(emailID);
    }

    //Method is for entering data into "First Name" field
    //@Test(priority = 3)
    public void enterDataFirstName(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering data in first name field");
        WebElement firstName = driver.findElement(By.xpath("//*[@id=\"first_name\"]"));
        JavascriptExecutor js1 = (JavascriptExecutor) driver;
        js1.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                firstName);
        firstName.click();
        firstName.clear();
        firstName.sendKeys("sample-firstname");

    }

    //Method is for entering data into "Last Name" field
    //@Test(priority = 4)
    public void enterDataLastName(WebDriver driver) {
        Reporter.log("Entering data in Last name field");
        WebElement lastName = driver.findElement(By.xpath("//*[@id=\"last_name\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                lastName);
        lastName.click();
        lastName.clear();
        lastName.sendKeys("sample-lastname");
    }


    //Method is for entering data into "Email Address" field
    //@Test(priority = 5)
    public void enterAddress(WebDriver driver) {
        Reporter.log("Entering  address");
        WebElement address = driver.findElement(By.xpath("//*[@id=\"address_1\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                address);
        address.click();
        address.clear();
        address.sendKeys("9021 Quartz Rd");

    }

    //Method is for entering data into "Email Address" field
    //@Test(priority = 6)
    public void enterCity(WebDriver driver) {
        Reporter.log("Entering  city");
        WebElement city = driver.findElement(By.xpath("//*[@id=\"city\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                city);
        city.click();
        city.clear();
        city.sendKeys("Whitehorse");

    }
    //Method is for entering data into "Email Address" field
    //@Test(priority = 7)
    public void enterState(WebDriver driver) throws InterruptedException {
        Reporter.log("Entering  state");
        WebElement state = driver.findElements(By.xpath("//div[contains(text(),'Select State')]")).get(0);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                state);
        state.click();
        Thread.sleep(2000);
        state.findElement(By.xpath("//div[contains(text(),'Yukon')]")).click();

    }

    //Method is for entering data into "Zip Code" field
    //@Test(priority = 8)
    public void enterZipCode(WebDriver driver) {
        Reporter.log("Entering Zip Code");
        WebElement ZipCode = driver.findElement(By.xpath("//*[@id=\"zipcode\"]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                ZipCode);
        ZipCode.click();
        ZipCode.clear();
        ZipCode.sendKeys("Y1A4P9");
    }

    //Method is for entering data into "Email Address" field
    //@Test(priority = 9)
    public void enterCountry(WebDriver driver) throws  InterruptedException {
        Reporter.log("Entering  country");
        WebElement country = driver.findElements(By.xpath("//div[contains(text(),'Country')]")).get(0);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 0px solid red;');",
                country);
        country.click();
        Thread.sleep(1000);
        country.findElements(By.xpath("//div[contains(text(),'Canada')]")).get(0).click();

    }

    //Method is for clicking the "Send My Message" button
    //@Test(priority = 10)
    public void submitForm(WebDriver driver) throws InterruptedException {
        String expectedSuccessUrl = "https://readyformore.com/ed-consultants?form=consultant&success=true#toolkit-request-success";
        Reporter.log("Submitting the form by clicking on Submit button");
        WebElement submit = driver.findElement(By.name("submit"));
        submit.click();
        Thread.sleep(4000);
        String actualsuccessUrl = driver.getCurrentUrl();
        System.out.println("actual:"+actualsuccessUrl);
        Assert.assertEquals(actualsuccessUrl, expectedSuccessUrl, "::::::::::::Send My Message submission was UNSUCCESSFULL:::::;");


    }

    //Method is for "closing the browser"
    //@Test(priority = 11)
    public void closebrowser(WebDriver driver) {
        driver.quit();

    }
}
