package FS_sanpchat;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;

public class FS_snapchatSignUp {
    WebDriver driver;

    //@Test(priority = 0)
    public void launchBrowser(WebDriver driver) {

        Reporter.log("This test will verify the firefox browser launch");

        System.out.println("launching firefox browser");
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.home") + "/bin/geckodriver");
        driver = new FirefoxDriver();
    }

    //Method is to open the "Readyformore.com website's contact us form" in the web browser
    //@Test(priority = 1)
    public void openApplication(WebDriver driver) throws InterruptedException {

        Reporter.log("This test will open the fusionSpan website on firefox");
        driver.get("http://snapchat.readyformore.com/signup");



    }

    //Method is to filloutform
    //@Test(priority = 2)
    public void filloutForm(WebDriver driver,String date) throws InterruptedException {

        Reporter.log("This test will fill out form");
        driver.findElement(By.name("firstname")).sendKeys("Test-archana");
        driver.findElement(By.name("lastname")).sendKeys("Test-patel");
        String emailID = "spudari+" + date + "scSignUp@fusionspan.com";
        driver.findElement(By.name("email")).sendKeys(emailID);
        driver.findElement(By.name("zipcode")).sendKeys("20893");


    }

    //Method is to submitform
    //@Test(priority = 3)
    public void submitForm(WebDriver driver) throws InterruptedException {

        Reporter.log("This test will submit the form");
        String expectedUrl ="https://readyformore.com/?form=email&success=true#global-footer";
        Thread.sleep(1000);
        driver.findElement(By.name("submit")).click();
        Thread.sleep(4000);
        String actualUrl =driver.getCurrentUrl();
        Assert.assertEquals(actualUrl, expectedUrl, "::::::::::::Send My Message submission was UNSUCCESSFULL:::::;");

    }

    //Method is for "closing the browser"
    //@Test(priority = 4)
    public void closebrowser(WebDriver driver) {
        driver.quit();
    }

}
